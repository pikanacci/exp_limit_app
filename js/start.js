$(function () {
    const REPETITION = 4;

    $.cookie("check_cookie", true);
    if (!$.cookie("check_cookie")) {
        alert("Please enable cookie before starting the experiment.");
    }

    /** Start the experiment **/
    $("#start_limit").click(function () {
        initialize();
        initLimit();
        //TODO: set random order to start experiment //
        window.location.href = "limit-1.html";
    });

    function initialize() {
        // alert("Start the experiment.");

        start = Date.now();
        id = "sy_" + generateStringRandomly();
        $.cookie("start", start);
        $.cookie("name", id);
        $.cookie("trial_count", 1);
        console.log(id);
        console.log(start);
    }
    function initLimit() {
        var condition = shuffleArray([0, 0, 100, 100]);  // todo
        // alert(condition);
        for (var i = 0; i < REPETITION; i++) {
            $.cookie("condition-" + String((i + 1)), condition[i]);
        }

        $.cookie("trial_max", REPETITION);
        $.cookie("experiment", "limit");
    }
   

    
    function generateStringRandomly() {
        var l = 6;
        // 生成する文字列に含める文字セット
        var c = "abcdefghijklmnopqrstuvwxyz0123456789";
        var cl = c.length;
        var r = "";
        for (var i = 0; i < l; i++) {
            r += c[Math.floor(Math.random() * cl)];
        }
        return r;
    }

    // https://murashun.jp/blog/20191110-24.html
    function shuffleArray(array) {
        for (var i = array.length - 1; i > 0; i--) {
            var rand = Math.floor(Math.random() * (i + 1));
            [array[i], array[rand]] = [array[rand], array[i]];
        }
        return array;
    }

    function randInt(start, end) {
        return start + Math.floor(Math.random() * (end - start + 1));
    }
});
